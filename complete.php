

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/main.css"/>
<title>Energize Greens Order Form</title>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>
<script>

	$(document).ready(function () {

		//select all the a tag with name equal to modal
		$('a[name=modal]').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();

			//Get the A tag
			var id = $(this).attr('href');

			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();

			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({ 'width': maskWidth, 'height': maskHeight });

			//transition effect		
			$('#mask').fadeIn(200);
			$('#mask').fadeTo("slow", 0.4);
			$('.close_icon').show();

			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();

			//Set the popup window to center
			$(id).css('top', winH / 2 - $(id).height() / 2);
			$(id).css('left', winW / 2 - $(id).width() / 2);

			//transition effect
			$(id).fadeIn(200);

		});


		//select all the a tag with name equal to modal
		$('a[name=modal1]').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();

			//Get the A tag
			var id = $(this).attr('href');

			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();

			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({ 'width': maskWidth, 'height': maskHeight });

			//transition effect		
			$('#mask').fadeIn(200);
			$('#mask').fadeTo("slow", 0.8);
			$('.close_icon').show();

			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();

			//Set the popup window to center
			$(id).css('top', winH / 2 - $(id).height() / 2);
			$(id).css('left', winW / 2 - $(id).width() / 2);

			//transition effect
			$(id).fadeIn(200);

		});

		//select all the a tag with name equal to modal
		$('a[name=modal2]').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();

			//Get the A tag
			var id = $(this).attr('href');

			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();

			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({ 'width': maskWidth, 'height': maskHeight });

			//transition effect		
			$('#mask').fadeIn(200);
			$('#mask').fadeTo("slow", 0.8);
			$('.close_icon').show();

			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();

			//Set the popup window to center
			$(id).css('top', winH / 2 - $(id).height() / 2);
			$(id).css('left', winW / 2 - $(id).width() / 2);

			//transition effect
			$(id).fadeIn(200);

		});

		//select all the a tag with name equal to modal
		$('a[name=modal3]').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();

			//Get the A tag
			var id = $(this).attr('href');

			//Get the screen height and width
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();

			//Set heigth and width to mask to fill up the whole screen
			$('#mask').css({ 'width': maskWidth, 'height': maskHeight });

			//transition effect		
			$('#mask').fadeIn(200);
			$('#mask').fadeTo("slow", 0.8);
			$('.close_icon').show();

			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();

			//Set the popup window to center
			$(id).css('top', winH / 2 - $(id).height() / 2);
			$(id).css('left', winW / 2 - $(id).width() / 2);

			//transition effect
			$(id).fadeIn(200);

		});

		//if close button is clicked
		$('.window .close').click(function (e) {
			//Cancel the link behavior
			e.preventDefault();

			$('#mask').hide();
			$('.window').hide();
		});

		//if mask is clicked
		$('#mask').click(function () {
			//$(this).hide();
			//$('.window').hide();
		});

		//if close is clicked
		$('.close_icon').click(function () {
			$(this).hide();
			$('.window').hide();
			$('#mask').hide();
		});

	});

</script>
<style>
	
	#mask
	{
		position: absolute;
		left: 0;
		top: 0;
		z-index: 9000;
		background-color: #000;
		display:block;
	}
	
	.close_icon
	{
		position: absolute;
		right: 0;
		top: 0;
		z-index: 9000; 
		cursor:pointer;
		margin:-15px -15px 0px 0px;
	}
	
	#boxes .window
	{
		position: absolute;
		left: 0;
		top: 0;
		display: none;
		z-index: 9999;
		text-align:left;
		padding:12px;
	}
	
	#boxes #dialog
	{
		width: 946px;
		height: auto;
		background-color: #ffffff;
		text-align:left;
		margin-top:50px;
	}
	
	#boxes1 .window
	{
		position: absolute;
		left: 0;
		top: 0;
		display: none;
		z-index: 9999;
		padding: 20px;
		text-align:left;
	}
	
	#boxes1 #dialog1
	{
		width: 600px;
		height: auto;
		padding: 20px;
		background-color: #ffffff;
		text-align:left;
		border:2px solid #2A66B1;
		margin-top:50px;
	}
	
	#boxes2 .window
	{
		position: absolute;
		left: 0;
		top: 0;
		display: none;
		z-index: 9999;
		padding: 20px;
		text-align:left;
	}
	
	#boxes2 #dialog2
	{
		width: 600px;
		height: auto;
		padding: 20px;
		background-color: #ffffff;
		text-align:left;
		border:2px solid #2A66B1;
		margin-top:50px;
	}
	
	#boxes3 .window
	{
		position: absolute;
		left: 0;
		top: 0;
		display: none;
		z-index: 9999;
		padding: 20px;
		text-align:left;
	}
	
	#boxes3 #dialog3
	{
		width: 600px;
		height: auto;
		padding: 20px;
		background-color: #ffffff;
		text-align:left;
		border:2px solid #2A66B1;
		margin-top:50px;
	}
	
	#boxes4 .window
	{
		position: absolute;
		left: 0;
		top: 0;
		display: none;
		z-index: 9999;
		padding: 20px;
		text-align:left;
	}
	
	#boxes4 #dialog4
	{
		width: 600px;
		height: auto;
		padding: 20px;
		background-color: #ffffff;
		text-align:left;
		border:2px solid #2A66B1;
		margin-top:300px;
	}
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64944063-1', 'auto');
  ga('send', 'pageview');

</script>




    <script language="javascript" type="text/javascript" src="js/jquery-1.4.4.min.js?12505898"></script>
    <script type="text/javascript">

        $(document).ready(function () {

            var parentUrl = top.location.href.split('/')[top.location.href.split('/').length - 1];

            if (parentUrl.toUpperCase() == "BD") {

                $('.hp-text').show();

            }

            $('.newsletter-text').focus(function () {

                if ($(this).val() == 'Enter Email') {

                    $(this).val('');

                }

            });

            $('.newsletter-text').blur(function () {

                if ($(this).val() == '') {

                    $(this).val('Enter Email');

                }

            });

        });
    </script>
    <script>
        var quotes = new Array()

        //change the quotes if desired. Add/ delete additional quotes as desired.

        quotes[0] = '<img src="http://mirorforex.alkahealth.hop.clickbank.net" width="0" height="0" />'
        quotes[1] = '<img src="http://georgetee.alkahealth.hop.clickbank.net" width="0" height="0" />'
        quotes[2] = '<img src="http://stoptinnit.alkahealth.hop.clickbank.net" width="0" height="0" />'
        quotes[3] = '<img src="http://tweetsys.alkahealth.hop.clickbank.net" width="0" height="0" />'
        quotes[4] = '<img src="http://teemarket.alkahealth.hop.clickbank.net" width="0" height="0" />'

        var whichquote = Math.floor(Math.random() * (quotes.length))
        document.write(quotes[whichquote])

        function showBuyLink() {
            document.getElementById("buylink").style.display = "block";
        }
        // adjust this as needed, 1 sec = 1000
        setTimeout("showBuyLink()", 395000);
    </script>
    <script type="text/javascript" src="js/jquery2.js"></script>
    <!-- javascript coding -->
    <script language="javascript" type="text/javascript">
//<![CDATA[
        $(document).ready(function () {
            $("#chained").scrollable({ circular: true, mousewheel: true }).navigator().autoscroll({
                interval: 3000
            });
        });
//]]>
    </script>
    <link rel="Stylesheet" type="text/css" href="box\jquery.fancybox-1.3.4.css" />
    <script language="javascript" type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
    <style type="text/css">
        .style3
        {
            color: #4A9E18;
            font-weight: bold;
            font-size: 18px;
        }
        .style5
        {
            color: #000000;
            font-size: 10px;
        }
    </style>

</head>

<body>
	<div id="content">
    	<div>
            <div class="fleft">
                <div class="pleft15">
                    <img src="images/logo.jpg" />
                </div>
                <div class="ptop10">
                    <img src="images/as-seen-on.jpg" />
                </div>
            </div>
            <div class="fright mtop15">
                <div id="nav">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <a href="index.html" class="pleft34">GET STARTED</a>
                            </td>
                            <td>
                                <img src="images/nav-divider.jpg" class="pleft34 pright34"/>
                            </td>
                            <td>
                                <a href="products.html" class="valign-top">PRODUCTS</a>
                            </td>
                            <td>
                                <img src="images/nav-divider.jpg" class="pleft34 pright34"/>
                            </td>
                            <td>
                                <a href="http://energizegreens.com/testimonials/index.html" class="valign-top">TESTIMONIALS</a>
                            </td>
                            <td>
                                <img src="images/nav-divider.jpg" class="pleft34 pright34"/>
                            </td>
                            <td>
                                <a href="newsletter.html" class="valign-top">NEWSLETTERS</a>
                            </td>
                            <td>
                                <img src="images/nav-divider.jpg" class="pleft34 pright34"/>
                            </td>
                            <td>
                                <a href="support.html" class="valign-top">SUPPORT</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="clear"></div>
        </div>
      <br /><br />
      <center>
      <div>
       <div id="index-02" style=" text-align:center; font-size:18px; font-weight:bold; color: #cc0000; font-family: arial">
                  	 <script>
              Date.prototype.addDays = function(days)
              {
               var dat = new Date(this.valueOf())
               dat.setDate(dat.getDate() + days);
               return dat;
              }
             
             var dat = new Date()
             
             var weekday=new Array(7);
             weekday[0]="Sunday";
             weekday[1]="Monday";
             weekday[2]="Tuesday";
             weekday[3]="Wednesday";
             weekday[4]="Thursday";
             weekday[5]="Friday";
             weekday[6]="Saturday";
             
             var n = weekday[dat.getDay() + 1];
             
             var dd = dat.getDate() + 1;
             var month=new Array();
             month[0]="January";
             month[1]="February";
             month[2]="March";
             month[3]="April";
             month[4]="May";
             month[5]="June";
             month[6]="July";
             month[7]="August";
             month[8]="September";
             month[9]="October";
             month[10]="November";
             month[11]="December";
             var mm = month[dat.getMonth()];
             var yy = dat.getFullYear();
			 
			 if(mm == "January" && dd == 32)
			 {
				mm = month[1];
				dd = 1;
			 }
			 
			 if(mm == "February" && dd == 29)
			 {
				mm = month[2];
				dd = 1;
			 }
			 
			 else if(mm == "March" && dd == 32)
			 {
				mm = month[3];
				dd = 1;	
			 }
			 
			 if(mm == "April" && dd == 31)
			 {
				mm = month[4];
				dd = 1;
			 }

			 
			 else if(mm == "May" && dd == 32)
			 {
				mm = month[5];
				dd = 1;	 
			 }
			 
			 if(mm == "June" && dd == 31)
			 {
				mm = month[6];
				dd = 1;
			 }
			 
			 else if(mm == "July" && dd == 32)
			 {
				mm = month[7];
				dd = 1;	 
			 }
			 
			 else if(mm == "August" && dd == 32)
			 {
				mm = month[8];
				dd = 1;	 
			 }
			 
			 if(mm == "September" && dd == 31)
			 {
				mm = month[9];
				dd = 1;
			 }
			 
			 else if(mm == "October" && dd == 32)
			 {
				mm = month[10];
				dd = 1;	
			 }
			 
			 if(mm == "November" && dd == 31)
			 {
				mm = month[11];
				dd = 1;
			 }
			 
			 else if(mm == "December" && dd == 32)
			 {
				mm = month[0];
				dd = 1;	 
			 }
             
             document.write( "Up to $70 OFF Regular Price Expires: " + n + ", " + mm + " " + dd + ", " + yy + "!");
         </script>
                  </div>
      	<div id="buylink">
        	<br/>
        	<div class="mleft100">
                <img src="images/special2.jpg" />
            </div>
            <br/>
        <center>
		<div class="bg-item-container">
        	<table cellpadding="0" cellspacing="0" class="pleft10">
                	<tr class="height190 taligncenter">
                    	<td style="width:388px;">
                        	<img src="images/item1.png" />
                        </td>
                        <td style="width:289px">
                        	<img src="images/price1.png" />
                        </td>
                        <td style="width:285px">
                        	<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GU39TA2ARXNHN">
                        		<img src="images/btn-add-to-cart-seal.png" />
                            </a>
                        </td>
                    </tr>
                    <tr class="height190 taligncenter">
                    	<td>
                        	<img src="images/item2.png" />
                        </td>
                        <td>
                        	<img src="images/price2.png" />
                        </td>
                        <td>
                        	<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=8SE27EU5K5YM6">
                        		<img src="images/btn-add-to-cart-seal.png" />
                            </a>
                        </td>
                    </tr>
                    <tr  class="height190 taligncenter">
                    	<td>
                        	<img src="images/item3.png" />
                        </td>
                        <td>
                        	<img src="images/price3.png" />
                        </td>
                        <td>
                        	<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LR9V25CJVFV4A" id="addtocart">
                        		<img src="images/btn-add-to-cart-seal.png" />
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <br/><br/>
            <div>
            	<img src="images/verifiedseal.jpg" />
            </div>
        </div>
      </div>
      </center>
        
            
            <div id="footer">
            <center>
        	<div>
            	<b style="font-family:Arial, Helvetica, sans-serif">© EnergizeGreens.com</b>
            </div>
            <div>
            <a href="http://www.energizegreens.com/refundpolicy.html" class="off">Refund Policy</a> |  <a href="#" class="off">Privacy Policy</a>  |  <a href="#" class="off">Terms of Use</a>
            | <a href="mailto: support@energizegreens.com" class="off">Contact</a> 
            </div>
            </center>
<div class="ptop20 talignleft">
            	The statements regarding these products have not been evaluated by the Food and Drug Administration. These products are not intended to diagnose, treat, cure or prevent any disease. The information on this Web site or in emails is designed for educational purposes only. It is not intended to be a substitute for informed medical advice or care. You should not use this information to diagnose or treat any health problems or illnesses without consulting your doctor.
                
                <br />
             <center>   <!-- START MCERTIFY CODE -->
<script id="mcertify" type="text/javascript">
 var wsid='1ee16835b6e6d19137fce98771e0e715';
 var s = document.getElementById('mcertify'); 
 var exScript = document.createElement('script');    
 exScript.type = 'text/javascript'; 
 exScript.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cert.verifystore.com/certs/js/xj_t.js';
 s.parentNode.insertBefore(exScript, s);   
</script>
<!-- END MCERTIFY CODE -->
</center>      
            </div>
        </div>
        </div>
        
    </div>
    <div id="mask">
    </div>
    <script language="javascript">
var exitsplashmessage = '***************************************\n\n W A I T   B E F O R E   Y O U   G O !\n\n  CLICK THE *CANCEL* BUTTON RIGHT NOW\n    To Get 3 Exclusive Health Reports!\n            (Worth $29.97)\n\n      \n***************************************';
var exitsplashpage = 'http://energizegreens.com/exitspecial/';
</script>
<script language="javascript" src="http://holistichealthlabs.com/exitsplash.php?tc=3399cc&uh=none&ad=none&sh=no&hv=no&bh=22&fs=12&lf=Arial&at=Powered%20by%20ExitSplash"></script>
</body>
</html>
