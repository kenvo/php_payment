<HTML>
<?php
//Sample PayPal Button Encryption: Copyright 2006-2010 StellarWebSolutions.com
//Not for resale  - license agreement at
//http://www.stellarwebsolutions.com/en/eula.php
//Updated: 2010 02 01

# private key file to use
$MY_KEY_FILE = __DIR__ . "/ssl/my-prvkey.pem";

# public certificate file to use
$MY_CERT_FILE = __DIR__ . "/ssl/my-pubcert.pem";

# Paypal's public certificate
$PAYPAL_CERT_FILE = __DIR__ . "/ssl/paypal_cert.pem";
// var_dump($MY_CERT_FILE);die();
# path to the openssl binary
$OPENSSL = "/usr/bin/openssl";

$form = array('cert_id' => '64JUWWQUNP84W',
    'subtotal'           => '50');

$encrypted = paypal_encrypt($form);
var_dump($encrypted);
function paypal_encrypt($hash)
{
    //Sample PayPal Button Encryption: Copyright 2006-2010 StellarWebSolutions.com
    //Not for resale - license agreement at
    //http://www.stellarwebsolutions.com/en/eula.php
    global $MY_KEY_FILE;
    global $MY_CERT_FILE;
    global $PAYPAL_CERT_FILE;
    global $OPENSSL;

    if (!file_exists($MY_KEY_FILE)) {
        echo "ERROR: MY_KEY_FILE $MY_KEY_FILE not found\n";
    }
    if (!file_exists($MY_CERT_FILE)) {
        echo "ERROR: MY_CERT_FILE $MY_CERT_FILE not found\n";
    }
    if (!file_exists($PAYPAL_CERT_FILE)) {
        echo "ERROR: PAYPAL_CERT_FILE $PAYPAL_CERT_FILE not found\n";
    }

    //Assign Build Notation for PayPal Support
    $hash['bn'] = 'StellarWebSolutions.PHP_EWP2';

    $data = "";
    foreach ($hash as $key => $value) {
        if ($value != "") {
            //echo "Adding to blob: $key=$value\n";
            $data .= "$key=$value\n";
        }
    }

    $openssl_cmd = "($OPENSSL smime -sign -signer $MY_CERT_FILE -inkey $MY_KEY_FILE " .
        "-outform der -nodetach -binary <<_EOF_\n$data\n_EOF_\n) | " .
        "$OPENSSL smime -encrypt -des3 -binary -outform pem $PAYPAL_CERT_FILE";

    exec($openssl_cmd, $output, $error);

    if (!$error) {
        return implode("\n", $output);
    } else {
        return "ERROR: encryption failed";
    }
};
?>

    <HEAD>
        <LINK REL=stylesheet HREF="/styles/stellar.css" TYPE="text/css">
        <TITLE>PHP Sample Donation using PayPal Encrypted Buttons</TITLE>
    </HEAD>

    <BODY bgcolor=white>
        <TABLE border=0>
            <TR>
                <TD align=center>
                    <h1>Sample Donation Page</h1>
                    <P>This page uses encrypted PayPal buttons for your security.</P>
                    <form action="https://securepayments.sandbox.paypal.com/webapps/HostedSoleSolutionApp/webflow/sparta/hostedSoleSolutionProcess" method="post">
                        <input type="hidden" name="cmd" value="_hosted-payment">
                        <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----
MIIHNQYJKoZIhvcNAQcDoIIHJjCCByICAQAxggE6MIIBNgIBADCBnjCBmDELMAkG
A1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExETAPBgNVBAcTCFNhbiBKb3Nl
MRUwEwYDVQQKEwxQYXlQYWwsIEluYy4xFjAUBgNVBAsUDXNhbmRib3hfY2VydHMx
FDASBgNVBAMUC3NhbmRib3hfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwu
Y29tAgEAMA0GCSqGSIb3DQEBAQUABIGAG6eM1aEaXtEzc+bhBari0FBaTRRZ7QgE
mvfbE6AsY+dgYpghj5NZy3mas8RJ8+wj6oRW0GNQ2thja22dln1BTPC0WksMZqoK
bA0YXncAn2mg7ui+Tkdgk+q19aeKBXVrhS+fX4zCQ+yIDXD9syRSg+IUlvNu0hmW
XXDCm2Wa6UcwggXdBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECMb6ZjSQ05WUgIIF
uNDlUYQYMNUkWz/GX8dB1r0JwVTyprXbuGtrb+Tv/CdctisIP9LBCu9ZALzRD6V1
JRUw/m6RXIorV6KV7Dei16cj5BFcAcTlSX8+6xuMsl/ZTLJ8hFDbO64JfA/E//Qh
NZTdHuL86wRyugjKF/9tI/IoVCS3fJCokHtLj+Dsv62nmRtY+yBMErCDyfgsYfsQ
N6Nyxtsb3f/PpfFyQDlKJFTRP+XQpXnMQq+ICTFGqyuwZBjIN+UXuPVzZefX1Xn2
2hv4o7/Iorm49KVo26CihfYpQ2hfArE73buQZtxbZEnsIA1BSLtsUUGuMMTXjC1T
ASjm9slHTYJDxZVam1CC/N1fNGssM+gkZNKlqhnYBRoEqYydjfvQXRVzF+dl69fn
XXHpHvQvzcXmZJSTHSTI0lKcXyX2iIlGDojoQaPy1dm9mICwRWi5HZy/zAVWEXLd
2Daam8NTGt7T3gYO+CgK72y4Sy8O5kpX2Wk1mjVqjlSPqAkOOes0OqTRnAvAzdIJ
gHQA2tlC2nRuaAJLotHkR3Hg0ZYoW1bUxqcazRv3Yp+p4kbTq6d/W3jawiOwOVZU
kgMzFT1HBOh1QLQ9+yFxbWR3HEOEf1c/lKRYPTlcvhkzBzJW8m3osVWObwtXIJsY
JbSeeUzDHz6VYJkV9pLYAlZCMswHwfjUp4Wzmc/N9pDEsBY5pd4jY5VtSpgk/P+i
vWJKGn2Mm5q6orxeTpnh/aNjjBvng1Pq7Jt0Gn6ujs03BjPOrWBmU50r/BUfV+KD
56d9KhZQDJ3C+yEN8i4mVI+54TFp/HFCXIOd2qBh5rUikBCf7jt6Tt8lcaPqQ7Qb
nL1rQKCdUXJo8D7JMZEOqlGQr5sxHmmNenl0MMGkXdfzpUpaMN1p0+by38rTx+Td
QDLw29e8jQtcXY1mnOfLYelAL44Q0I6HuIcZKEPlhtnBkHHRYPLglR+BOHhNWZ5i
tHMJl7du3Mo7FuP5zZKNTg8yX+WFFKkg0Ou6wz9I7huJCY16xhZP+9dSIXMyjIEK
xjDAEVlJiPajGOg6ayTm0kvOpkwHCSLQukLmEaCf1BAWhV7PD3sb365TyQ07Uc1k
+E8Wx509J+xglWFWwy9cs79KPVKpQltsp5GZeNJc1C0YhsvvHXVzBBiC4DnyMZWr
ypaq4M8ykJl84i0296T47GTbfDHAL63hdsBTZzrF+ofF9gX6d6wPyXPpkPg06IaU
OYzACpGe/miTS9EGaQqHerpCvkh6LM2/5X3mqkfwVcnFvb+Q6jz18u01tn3UcGmh
jkfZiNy14RniNaMH9hGbSv7/pzOwsyidhjbuUYuNS9DryxsbMtvq5TJ1zrUJCfps
Dr1n8thY/aXykkfoO9TPHTCfVwszjNg7b/XG5iLaIXSMSmFPtxn7DE2ClTWcAmmy
dktm6flt2NJl1Hglscnf5b3W9cViQ5ak7DuMBvVqEvRbvmh3/0UGmAqdJTYhO+Hp
rkLS0DR4GyfqB8F/Rbkgd8HXNQIFyPNuN2Z850x8LHUFss6BBmW3yBp92wHc4UXk
MoFDuX6f/pNIwscTYVpsN6aeiFjmavNEFr5CrwFUMJty80MiiW1YRRkP2N7TMVQt
Ub+nuCABwZpyxy820heAAyeE2F11O01wpgImJy59sMaWaJ5hKXWidSei2udkcraR
kF/gIB//f0EOV17TLKXKiwowjLrs0wpTlG0LDQEopqiu7sC0OWt0D03ZKWK7g4jn
EgYl/HI37lk+eOsW8OlS0Poejw5HlSDdthP2+dBh3l4LDPxG2bz+TR5pdYG0D6Iu
6ksGcCYYCrY4vAhLfHxnKjJtH9DZIr/EVlcysPohxers2LEUFpwu49OelTujTyH1
gSi05Wh3rFyrsszlyqGneCIVSE4RH/RJFOdFzN1Nn1suzoRJHOIHpSJD9AEfwAFl
iXDyETtAmytFd4ZaCmrKwYlvPtU9nRoy2g==
-----END PKCS7-----">
<input type="hidden" name="business" value="ZFSHJF7K8VACG">
                        <input type="submit" name="METHOD" value="Pay Now">
                    </form>
					
					 <!--input type="hidden" name="encrypted" value="<?php echo $encrypted; ?>"-->
    </BODY>

</HTML>
