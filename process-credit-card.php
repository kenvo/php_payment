<?php
require_once 'config.php';

// Tạo yêu cầu API và ưu các tham số đó vào mảng
$request_params = array(
    'METHOD'         => 'DoDirectPayment',
    'USER'           => $api_username,
    'PWD'            => $api_password,
    'SIGNATURE'      => $api_signature,
    'VERSION'        => $api_version,
    'PAYMENTACTION'  => 'Sale',
    'IPADDRESS'      => $_SERVER['REMOTE_ADDR'],
    'CREDITCARDTYPE' => 'Visa',
    'ACCT'           => '403203589898212321321020',
    'EXPDATE'        => '082020',
    'CVV2'           => '020',
    'FIRSTNAME'      => 'Yang',
    'LASTNAME'       => 'Ling',
    'STREET'         => '1 Main St',
    'CITY'           => 'San Jose',
    'STATE'          => 'CA',
    'COUNTRYCODE'    => 'US',
    'ZIP'            => '95131',
    'AMT'            => '10.00',
    'CURRENCYCODE'   => 'USD',
    'DESC'           => 'Testing Payments Pro',
);

// vòng lặp với mảng $request_params để tạo chuỗi NVP (Name-Value Pair).

$nvp_string = '';

foreach ($request_params as $var => $val) {
    $nvp_string .= '&' . $var . '=' . urlencode($val);
}
// gửi yêu cầu (chuỗi NVP ) HTTP đến PayPal

$curl = curl_init();

curl_setopt($curl, CURLOPT_VERBOSE, 0);

curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

curl_setopt($curl, CURLOPT_TIMEOUT, 30);

curl_setopt($curl, CURLOPT_URL, $api_endpoint);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

// những thông tin trên được gửi qua Paypal và tôi sẽ nhận được phản hồi trong biến $result

$result = curl_exec($curl);

curl_close($curl);

// Phân tách chuỗi phản hồi dùng hàm parse_str()

$nvp_response_array = parse_str($result);

// Hàm chuyển chuỗi NVP sang dạng mảng

function NVPToArray($NVPString)
{

    $proArray = array();

    while (strlen($NVPString)) {
        // key

        $keypos = strpos($NVPString, '=');

        $keyval = substr($NVPString, 0, $keypos);

        //value

        $valuepos = strpos($NVPString, '&') ? strpos($NVPString, '&') : strlen($NVPString);
        $valval   = substr($NVPString, $keypos + 1, $valuepos - $keypos - 1);

        // giải mã chuỗi phản hồi
        $proArray[$keyval] = urldecode($valval);
        $NVPString         = substr($NVPString, $valuepos + 1, strlen($NVPString));
    }
    return $proArray;
}
$result_array = NVPToArray($result);

// hiển thị dạng phản hồi API theo mảng
echo '<pre />';
print_r($result_array);
