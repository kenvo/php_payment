<HTML>
<?php
//Sample PayPal Button Encryption: Copyright 2006-2010 StellarWebSolutions.com
//Not for resale  - license agreement at
//http://www.stellarwebsolutions.com/en/eula.php
//Updated: 2010 02 01

# private key file to use
$MY_KEY_FILE = __DIR__ . "/ssl/my-prvkey.pem";

# public certificate file to use
$MY_CERT_FILE = __DIR__ . "/ssl/my-pubcert.pem";

# Paypal's public certificate
$PAYPAL_CERT_FILE = __DIR__ . "/ssl/paypal_cert.pem";
// var_dump($MY_CERT_FILE);die();
# path to the openssl binary
$OPENSSL = "/usr/bin/openssl";

$form = array('cmd' => '_xclick',
        'business' => 'vicoders.test@gmail.com',
        'cert_id' => '64JUWWQUNP84W',
        'lc' => 'HK',
        'custom' => 'test',
        'invoice' => '',
        'currency_code' => 'HKD',
        'no_shipping' => '1',
        'item_name' => 'Donation',
        'item_number' => '1',
	'amount' => '10'
	);

$encrypted = paypal_encrypt($form);
var_dump($encrypted);
function paypal_encrypt($hash)
{
    //Sample PayPal Button Encryption: Copyright 2006-2010 StellarWebSolutions.com
    //Not for resale - license agreement at
    //http://www.stellarwebsolutions.com/en/eula.php
    global $MY_KEY_FILE;
    global $MY_CERT_FILE;
    global $PAYPAL_CERT_FILE;
    global $OPENSSL;

    if (!file_exists($MY_KEY_FILE)) {
        echo "ERROR: MY_KEY_FILE $MY_KEY_FILE not found\n";
    }
    if (!file_exists($MY_CERT_FILE)) {
        echo "ERROR: MY_CERT_FILE $MY_CERT_FILE not found\n";
    }
    if (!file_exists($PAYPAL_CERT_FILE)) {
        echo "ERROR: PAYPAL_CERT_FILE $PAYPAL_CERT_FILE not found\n";
    }

    //Assign Build Notation for PayPal Support
    $hash['bn'] = 'StellarWebSolutions.PHP_EWP2';

    $data = "";
    foreach ($hash as $key => $value) {
        if ($value != "") {
            //echo "Adding to blob: $key=$value\n";
            $data .= "$key=$value\n";
        }
    }

    $openssl_cmd = "($OPENSSL smime -sign -signer $MY_CERT_FILE -inkey $MY_KEY_FILE " .
        "-outform der -nodetach -binary <<_EOF_\n$data\n_EOF_\n) | " .
        "$OPENSSL smime -encrypt -des3 -binary -outform pem $PAYPAL_CERT_FILE";

    exec($openssl_cmd, $output, $error);

    if (!$error) {
        return implode("\n", $output);
    } else {
        return "ERROR: encryption failed";
    }
};
?>

    <HEAD>
        <LINK REL=stylesheet HREF="/styles/stellar.css" TYPE="text/css">
        <TITLE>PHP Sample Donation using PayPal Encrypted Buttons</TITLE>
    </HEAD>

    <BODY bgcolor=white>
        <TABLE border=0>
            <TR>
                <TD align=center>
                    <h1>Sample Donation Page</h1>
                    <P>This page uses encrypted PayPal buttons for your security.</P>
                    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" >
<input type="hidden" name="cmd" value="_s-xclick">
                      
                        <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----
MIIHtQYJKoZIhvcNAQcDoIIHpjCCB6ICAQAxggE6MIIBNgIBADCBnjCBmDELMAkG
A1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExETAPBgNVBAcTCFNhbiBKb3Nl
MRUwEwYDVQQKEwxQYXlQYWwsIEluYy4xFjAUBgNVBAsUDXNhbmRib3hfY2VydHMx
FDASBgNVBAMUC3NhbmRib3hfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwu
Y29tAgEAMA0GCSqGSIb3DQEBAQUABIGAV2UIxEn31VONmyQ9YvuizU2u/8Wc5Y8g
9vg9avg6WswO36ZmgJSvbDu9nKA4wgAN1d6Ah9Ii5RDseRC9grpWIsvtjOOO2YeU
ke9oXTiVcBhchR6KMcdKve+LACjIIVfD2pzfwoOTJpnfgMT99AhyopJp0diDNylj
IDLxsLXFu88wggZdBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECBXSZTo+sRdHgIIG
OCehLg7MRymxSS7lPZOg7YdIJGvkkWL2pgXv6H/v7g+nhfBGrEKBAeUePHUOikfK
x0sOlt3Wq4OJT21bI7HiB+WfLh+q8KIaCydXuHl7lK+NQ950A+HlBaRR6dOXwjr/
nGkuRH9kHOENH/f8GeI3tepKgkos7VFyAQ29HSSeABVzwRXZb0cO5I0huJghrHo/
H5E0fi3IMfiuzKaw2uabRqlsslZHlwoWFt78ZBOPhIWyV6RHpHw8lYBmYbZ6+8Ce
0UazEH1wAUMBtzkCVwOKr9PnGkk+tmmGiG2UnMVsshqIfVEdx/E5Ivy+ffQ5/yf4
gADz/r2t0AuDUjrvT/wb6QQqZV3mChZ8qgl0Ey6uEYJCRLJD0dMjwUE2GPkVyRzt
plFgMzuanQS2BXQx/NpC3oZTVkbnIdHrzhtGqw/osO5s6fHDC0oUooVw2+E5hlqW
mvCeKPLN+0mzfZFhQuE6OZRMmX3GpUo0Vwdz6OvLmRghVO3PbZ7Kz7MEeeFCwyri
E0gz7k5f0sjx8penRdHG63UTYhuPYm2r7tlTzf7kj9sviWdgBihPaP2M9UbB8g4k
ZhzSCl4FQJj/aZuGbNoJCOksOgEMEsHuDlHetJspEFT8+JN7wc1SsikyMoGgV0MX
Z0gtCRazjITADes/avOo2BqSzIUbtRodRiKe9CSQIoVNGHa5wM2smynqeETA4naK
XZ6WL29dS3xmairswZctQrQtP8OAuNKM7YrdUG7HXxiJysy5z1IQwJTfCyPP7RxM
ZE9mjM/Q3XNC5yOFjaPOy2XDOGdmPhmDYzWgr5q+Ri/W9jviwEwdq6lpIkSx8JuJ
EYAJ3+7bG2xCZVtmu7ny28r48ozUxH7eAsjehoPXjymFv5wkelc2N4G7ylpF2Vo8
fue5bK7142zT5ZloC0B+imhcWuSq0iT9YOB/hVEwFaRpr01AQi/HtaIIOufkK/k/
HCsMq5jGPU+/pQutleT8j+6Ux4t5RLcBbKQb1G9Hj4F3jU2685RaiEn6i6VXjFoq
gVMZljm0N0CqDpxDgWQPSi3iZtCoY0htFW1Qwq2rtQ5S7h2t9NjCplqHcRzfnnPX
XMysAnKyeFE6u83E/TTiRxDv9S39H72JaX68Y7qJ8/wxoB7Rh/wFgI47/b3GgUk9
vSQEpxLPaGEWWyUqoQ7AVHS1GovSRwKI9HcjhZzkpxtfgaM69Hf5YUy6JSadouhr
9mG24IOxPGz/5Pf7Nj4drrV7Jmaio4LtvGRFwUtUCVigLCg++/rz+2JuTgYpcIHm
IyP8jjzvJgGErzPabQDszp/qLmDv7Rb3AQY5hW8RvkUr9n0Ri9oNnC/Y2qmjdLgR
t1dk37nZEWVLFMgOJhK9j1sUVHJWo3TGLji5fbltMcQuGskF3H7RaVx8N9ofIDrJ
9/XXyWAYRW18Kodb0WMt6meS6O5o7qCqbNDH0bL2l8FSBV5hOGqqqRt3FzpGT4pm
8AJ3htq99lv18BLkpib8rW8tAif7FA2BFhl+b0zn5b5naLhSoSIV3LwO2b0QUlNf
aBY/60R3L2P0Ag//tV/iVPR/EixLy27Koak4neoPohtEGGlqS5AphEtE57iilEoh
m53Gai4LvNe4q9+JmUc3dTix7Hbfx3asy2P3XD00yKd3WFQcllRfRgFKWWIHn1dr
t6k3mMVxsshSVbM7GeEW9AT1a4VDMEGoZtCrjzy6zYQaVEpV//4WcCiCC36c7yMl
fcb2tHHsDoGj0LRJ5y7v0gGhthK8R0hwxX05ugW23WCBVSuRqcIu1Zsq9mXNhwy6
ejVmRtZD6lEBKqjEw133m4+J2tUw615rz0NFkrBjO9MgIerCPwUZ2CCZguAsns7V
aTRJUEJISGF5FkgbIkefQzKWaIlBYi9WM9dEh6d/VJ/rRE4g8Jrg1kvRNEjlsuRo
eaqZJZbadsQhmVDw5bzega3iD+4dnnxswsokKxaQc7dKjxlvrkv33aQdbcXZ2JI+
wS6jXkOP434Gsy4bRZPbFcXnzPaQOLMvfr/5BEyqNG+edcQ5TMCsDb+n9VO7FqEh
UhvpE48c/W6LfOvJ99UMJy1tE7nEZ0WE+FmAAyqXUBQWF8gqQrkKUN2eo+i13CDr
oT6KUTjb4qI5
-----END PKCS7-----">
                        <input type="submit" name="METHOD" value="Pay Now">
                    </form>
					
					  <!--input type="hidden" name="encrypted" value="<?php echo $encrypted; ?>"-->
    </BODY>

</HTML>
