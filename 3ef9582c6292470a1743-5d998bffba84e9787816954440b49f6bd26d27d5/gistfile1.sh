#!/bin/bash

echo "All certificates in ca-certificates.crt, listed by subject:"
awk -v cmd='openssl x509 -noout -subject' '/BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt

echo "All certificates in ca-certificates.crt, listed by subject, check for presence of VeriSign's 'Class 3 Public Primary - G5':"
awk -v cmd='openssl x509 -noout -subject' '/BEGIN/{close(cmd)};{print | cmd}' < /etc/ssl/certs/ca-certificates.crt | grep "G5"